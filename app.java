import java.lang.Math;

public class app {
	public boolean isPrime(long number){
		if(number==1) return false;
		if(number ==2 )return true;
		for(long i=2;i<Math.sqrt(number)+1;i++){
			if(number%i==0) return false;	
		}
		return true;
	}
	public static void main(String[] args){
		if(args.length <1 || args.length>1){
			System.out.println("plz insert exacly one input");
			System.exit(0);
		}
		app a = new app();
		try{
			long in = Long.parseLong(args[0]);
			System.out.println(a.isPrime(in));
		}catch(Exception e){
			System.out.println("numberic is accepted");
		}
	}

}
