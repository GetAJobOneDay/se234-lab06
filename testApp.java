import java.util.*;
import static org.junit.Assert.*;
import org.junit.*;
public class testApp{
	private app a;
	@Before
	public void setup(){
		a = new app();
	}
	@Test
	public void testSamllPrime(){
		assertEquals(true,a.isPrime(17));
	}
	@Test
	public void testSmallNotPrime(){
		assertEquals(false,a.isPrime(33));
	}
	@Test
	 public void testBigPrime(){
		assertEquals(true,a.isPrime(32416189049L));
	}
	@Test
	public void testBigNotPrime(){
		assertEquals(false,a.isPrime(32416189051L));
	}
	       

}
